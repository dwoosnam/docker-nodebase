FROM node:5

MAINTAINER David Woosnam "david@thewoo.co.uk"

# Version in case anyone wants it
ENV NODEBASE_VERSION 0.0.1

# A default node environment
ENV NODE_ENV woosteln_nodebase

# Add bower just because it might be useful.. Multipurpose this
RUN npm install -g bower

# Create a mount point for node apps
RUN mkdir /var/opt/nodesrc

# Allow parameterization of our entry point ( some may use app.js etc )
# Todo. Modify CMD at end of file to use this
ENV NODE_ENTRY_POINT index.js

# Add test files
# ADD . /var/opt/nodesrc/

# Add an entry point to run npm install
COPY docker_entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Listen to the outside world
EXPOSE 80 443

WORKDIR "/var/opt/nodesrc"

ENTRYPOINT [ "/entrypoint.sh" ]

CMD [ "node", "index.js" ]
