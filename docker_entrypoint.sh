#!/bin/bash

set -e

DIRECTORY="/var/opt/nodesrc"
NODE_MODULES="$DIRECTORY/node_modules"
NODE_PACKAGE="$DIRECTORY/package.json"
BOWER_PACKAGE="$DIRECTORY/bower.json"

if [ -e $NODE_PACKAGE ]; then

  if [ ! -d $NODE_MODULES ]; then
    # Control will enter here if $DIRECTORY doesn't exist.
    # Ensure we're in the node dir
    cd $DIRECTORY
    # Run npm install
    npm install
  fi

fi

if [ -e $NODE_PACKAGE ]; then

  cd $DIRECTORY
  # Run bower install
  bower install --config.interactive=false --allow-root

fi

# Revert file permissions to what they were initially.
# Only useful if exposed as volume
# chown -R `stat -c %u:%g /var/opt/nodesrc` /var/opt/nodesrc

exec "$@"
